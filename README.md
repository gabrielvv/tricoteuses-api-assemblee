# Tricoteuses-API-Assemblee

## _API GraphQL pour Tricoteuses basée sur les données open data de l'Assemblée nationale_

## Utilisation

### Récupération quotidienne des données

Ce script récupère les différents fichiers JSON du site [open data de l'Assemblée nationale](http://data.assemblee-nationale.fr/). Il les décompresse et les réindente (car par défaut ils tiennent sur une ligne, ce qui ne facilite pas le debogage) :

```bash
cargo run -p tricoteuses_api_assemblee_open_data_fetcher -- -c Config.toml -v
```

### Découpe des données en petits fichiers

Cette étape n'est pas strictement nécessaire pour Tricoteuses, mais elle permet de mieux voir les changements se produisant d'un jour à l'autre dans les données. La découpe en petit fichier permet par exemple d'avoir un historique Git utilisable.

```bash
cargo run -p tricoteuses_api_assemblee_open_data_splitter -- -c Config.toml -v
```


### Récupération quotidienne des photos des députés

Ce script utilise les fichiers JSON précédents pour récupérer les photos des députés sur le site de l'Assemblée nationale, puis il retaille certaines photos (afin qu'elles soient toutes aux mêmes dimensions), génère une mosaïque contenant toutes les photos et enfin produit un fichier JSON contenant les informations sur les photographies de chaque député.

```bash
cargo run -p tricoteuses_api_assemblee_photos_fetcher -- -c Config.toml -v
```

### Lancement du serveur web

La commande ci-dessous, charge en mémoire tous les fichiers JSON précédentes et lance un serveur web proposant une API GraphQL, ainsi que les photos des députés :

```bash
cargo run -p tricoteuses_api_assemblee -- -c Config.toml -v
```

Pour tester le service GraphQL avec l'interface GraphiQL, ouvrir l'URL `http://localhost:8000` dans un navigateur.

Pour voir une mosaïque des photos des députés : `http://localhost:8000/assets/photos_deputes_15/deputes.jpg`.
