use crate::acteurs::Acteur;
use crate::contexts::Context;
use crate::serde_utils::str_to_vec;
use crate::textes_legislatifs::Document;

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Chrono {
    // TODO: Some date have a wrong format: `2017-07-20T0017:30.000+02:00` instead of `2017-07-20T17:30:00.000+02:00`
    // cloture: Option<DateTime<FixedOffset>>,
    cloture: Option<String>,
    // TODO: Some date have a wrong format: `2017-07-20T0017:30.000+02:00` instead of `2017-07-20T17:30:00.000+02:00`
    // creation: DateTime<FixedOffset>,
    creation: String,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Codier {
    pub fam_code: String,
    pub libelle: String,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct CycleDeVie {
    pub chrono: Chrono,
    pub etat: String,
}

// #[derive(Clone, Debug, Deserialize)]
// #[serde(deny_unknown_fields)]
// pub struct EmptyChoice {}

// graphql_scalar!(EmptyChoice {
//     resolve(&self) -> juniper::Value {
//         juniper::Value::Null
//     }

//     from_input_value(v: &InputValue) -> Option<EmptyChoice> {
//         None
//     }

//     from_str<'a>(value: ScalarToken<'a>) -> juniper::ParseScalarResult<'a, juniper::DefaultScalarValue> {
//         Err(juniper::parser::ParseError::UnexpectedToken(juniper::parser::Token::Scalar(value)))
//     }
// });

pub type EmptyChoice = bool;

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Ident {
    pub alpha: Option<String>,
    pub civ: String,
    pub nom: String,
    pub prenom: String,
    pub trigramme: Option<String>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Indexation {
    pub themes: Themes,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct InfoJo {
    #[serde(rename = "typeJO")]
    pub type_jo: TypeJo,
    // TODO: Date<FixedOffset> type is not able to deserialize dates with timezone (`YYYY-MM-DD±HH:MM`).
    #[serde(rename = "dateJO")]
    pub date_jo: String,
    #[serde(rename = "pageJO")]
    pub page_jo: Option<EmptyChoice>,
    #[serde(rename = "numJO")]
    pub num_jo: String,
    #[serde(rename = "urlLegifrance")]
    pub url_legifrance: Option<String>,
    #[serde(rename = "referenceNOR")]
    pub reference_nor: Option<String>,
}

#[derive(Clone, Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct ListePays {
    #[serde(deserialize_with = "str_to_vec", rename = "paysRef")]
    pub pays_ref: Vec<String>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Rapporteur {
    #[serde(rename = "acteurRef")]
    pub acteur_ref: String,
    #[serde(rename = "etudePLFRef")]
    pub etude_plf_ref: Option<String>,
    #[serde(rename = "typeRapporteur")]
    pub type_rapporteur: TypeRapporteur,
}

graphql_object!(Rapporteur: Context |&self| {
    field acteur(&executor) -> Option<&Acteur> {
        let context = &executor.context();
        context.get_acteur_at_uid(&self.acteur_ref)
    }

    field acteur_ref() -> &str {
        &self.acteur_ref
    }

    field etude_plf(&executor) -> Vec<&Document> {
        match &self.etude_plf_ref {
            None => vec![],
            Some(ref etude_plf_ref) => {
                let context = &executor.context();
                match context.get_document_at_uid(&etude_plf_ref) {
                    None => vec![],
                    Some(ref document) => {
                        // Generate a flat vector from document and its sub-documents (aka divisions),
                        // because a tree is not adapted to GraphQL.
                        document.flatten_document()
                    },
                }
            },
        }
    }

    field etude_plf_ref() -> Option<&str> {
        match &self.etude_plf_ref {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field type_rapporteur() -> TypeRapporteur {
        self.type_rapporteur
    }
});

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Theme {
    #[serde(rename = "libelleTheme")]
    pub libelle_theme: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Themes {
    #[serde(rename = "@niveau")]
    pub niveau: String, // TODO: Convert to u32.
    pub theme: Theme,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum TypeJo {
    #[serde(rename = "JO_DEBAT")]
    Debat,
    #[serde(rename = "JO_QUESTION")]
    Question,
    #[serde(rename = "JO_LOI_DECRET")]
    LoiDecret,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum TypeRapporteur {
    #[serde(rename = "rapporteur")]
    Rapporteur,
    #[serde(rename = "rapporteur général")]
    RapporteurGeneral,
    #[serde(rename = "rapporteur pour avis")]
    RapporteurPourAvis,
    #[serde(rename = "rapporteur spécial")]
    RapporteurSpecial,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Uid {
    #[serde(rename = "#text")]
    pub text: String,
    #[serde(rename = "@xsi:type")]
    pub type_uid: String,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum XmlNamespace {
    #[serde(rename = "http://www.w3.org/2001/XMLSchema-instance")]
    XmlSchemaInstance,
}
