// use crate::actes_legislatifs::ActeLegislatif;
use crate::dossiers_legislatifs::ExportJsonWrapper;

pub fn check_data(dossiers_legislatifs_wrapper: &ExportJsonWrapper) {
    for dossier in &dossiers_legislatifs_wrapper
        .export
        .dossiers_legislatifs
        .dossiers
    {
        assert!(!dossier
            .dossier_parlementaire
            .actes_legislatifs()
            .actes
            .is_empty());
        // let max_depth = max_depth_actes_legislatifs(dossier_parlementaire.actes_legislatifs().actes);
        // assert!(max_depth <= 4, "Profondeur maximale des actes législatifs du dossier parlementaire \"{}\" = {}", dossier_parlementaire.titre_dossier().titre, max_depth);
    }
}

// fn max_depth_actes_legislatifs(actes_legislatifs: &[ActeLegislatif]) -> u32 {
//     match itertools::max(
//         actes_legislatifs
//             .iter()
//             .map(|acte_legislatif| {
//                 1 + (match &acte_legislatif.actes_legislatifs {
//                     None => 0,
//                     Some(actes_legislatifs) => max_depth_actes_legislatifs(&actes_legislatifs.actes),
//                 })
//             })
//         ) {
//         None => 0,
//         Some(max) => max,
//     }
// }
