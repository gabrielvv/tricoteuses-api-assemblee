use crate::commun::{EmptyChoice, XmlNamespace};
use crate::contexts::Context;
use crate::serde_utils::{map_to_vec, str_to_option_bool};

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum CausePositionVote {
    #[serde(rename = "MG")]
    MembreGouvernement,
    #[serde(rename = "PP")]
    PositionPersonnelle,
    #[serde(rename = "PAN")]
    PresidentAssembleeNationale,
    #[serde(rename = "PSE")]
    PresidentSeance,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct DecompteMiseAuPoint {
    // Bug in data.
    abstentions: (Option<EmptyChoice>, Option<DecompteVotants>),
    contres: Option<DecompteVotants>,
    dysfonctionnement: Dysfonctionnement,
    // Bug in data.
    #[serde(rename = "nonVotants")]
    non_votants: (
        Option<EmptyChoice>,
        Option<DecompteVotantsAvecMotivationVote>,
    ),
    // Bug in data.
    #[serde(rename = "nonVotantsVolontaires")]
    non_votants_volontaires: (
        Option<EmptyChoice>,
        Option<DecompteVotantsAvecMotivationVote>,
    ),
    pours: Option<DecompteVotants>,
}

graphql_object!(DecompteMiseAuPoint: Context |&self| {
    field abstentions() -> &Option<DecompteVotants> {
        &self.abstentions.1
    }

    field contres() -> &Option<DecompteVotants> {
        &self.contres
    }

    field non_votants() -> &Option<DecompteVotantsAvecMotivationVote> {
        &self.non_votants.1
    }

    field non_votants_volontaires() -> &Option<DecompteVotantsAvecMotivationVote> {
        &self.non_votants.1
    }

    field pours() -> &Option<DecompteVotants> {
        &self.pours
    }
});

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct DecompteNominatif {
    abstentions: Option<DecompteVotants>,
    contres: Option<DecompteVotants>,
    #[serde(rename = "nonVotants")]
    non_votants: Option<DecompteVotantsAvecMotivationVote>,
    pours: Option<DecompteVotants>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct DecompteVoix {
    abstentions: String,
    contre: String,
    #[serde(rename = "nonVotants")]
    non_votants: String,
    #[serde(rename = "nonVotantsVolontaires")]
    non_votants_volontaires: String,
    pour: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct DecompteVotants {
    #[serde(deserialize_with = "map_to_vec", rename = "votant")]
    pub votants: Vec<Votant>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct DecompteVotantsAvecMotivationVote {
    #[serde(deserialize_with = "map_to_vec", rename = "votant")]
    pub votants: Vec<VotantAvecMotivationVote>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Demandeur {
    #[serde(rename = "referenceLegislative")]
    reference_legislative: Option<EmptyChoice>,
    texte: Option<String>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Dysfonctionnement {
    abstentions: Option<DecompteVotants>,
    contre: Option<DecompteVotants>,
    #[serde(rename = "nonVotants")]
    non_votants: Option<DecompteVotants>,
    #[serde(rename = "nonVotantsVolontaires")]
    non_votants_volontaires: Option<DecompteVotants>,
    pour: Option<DecompteVotants>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Groupe {
    #[serde(rename = "nombreMembresGroupe")]
    nombre_membres_groupe: String,
    #[serde(rename = "organeRef")]
    organe_ref: String,
    vote: VoteGroupe,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Groupes {
    #[serde(deserialize_with = "map_to_vec", rename = "groupe")]
    pub groupes: Vec<Groupe>,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum ModePublicationDesVotes {
    DecompteNominatif,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Objet {
    libelle: String,
    #[serde(rename = "referenceLegislative")]
    reference_legislative: Option<EmptyChoice>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Organe {
    groupes: Groupes,
    #[serde(rename = "organeRef")]
    organe_ref: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct OrganeVote {
    organe: Organe,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Scrutin {
    #[serde(rename = "dateScrutin")]
    // TODO: Date<FixedOffset> type is not able to deserialize dates.
    pub date_scrutin: String,
    pub demandeur: Demandeur,
    pub legislature: String,
    #[serde(rename = "miseAuPoint")]
    pub mise_au_point: Option<DecompteMiseAuPoint>,
    #[serde(rename = "modePublicationDesVotes")]
    pub mode_publication_des_votes: ModePublicationDesVotes,
    pub numero: String,
    pub objet: Objet,
    #[serde(rename = "organeRef")]
    pub organe_ref: String,
    #[serde(rename = "quantiemeJourSeance")]
    pub quantieme_jour_seance: String,
    #[serde(rename = "seanceRef")]
    pub seance_ref: String,
    #[serde(rename = "sessionRef")]
    pub session_ref: String,
    pub sort: SortScrutin,
    #[serde(rename = "syntheseVote")]
    pub synthese_vote: SyntheseVote,
    pub titre: String,
    #[serde(rename = "typeVote")]
    pub type_vote: TypeVote,
    pub uid: String,
    #[serde(rename = "ventilationVotes")]
    pub ventilation_votes: OrganeVote,
    #[serde(rename = "@xmlns:xsi")]
    pub xml_xsi: Option<XmlNamespace>,
}

graphql_object!(Scrutin: Context |&self| {
    field date_scrutin() -> &str {
        &self.date_scrutin
    }

    field demandeur() -> &Demandeur {
        &self.demandeur
    }

    field legislature() -> &str {
        &self.legislature
    }

    field mise_au_point() -> &Option<DecompteMiseAuPoint> {
        &self.mise_au_point
    }

    field mode_publication_des_votes() -> &ModePublicationDesVotes {
        &self.mode_publication_des_votes
    }

    field organe_ref() -> &str {
        &self.organe_ref
    }

    field quantieme_jour_seance() -> &str {
        &self.quantieme_jour_seance
    }

    field seance_ref() -> &str {
        &self.seance_ref
    }

    field session_ref() -> &str {
        &self.session_ref
    }

    field sort() -> &SortScrutin {
        &self.sort
    }

    field synthese_vote() -> &SyntheseVote {
        &self.synthese_vote
    }

    field titre() -> &str {
        &self.titre
    }

    field type_vote() -> &TypeVote {
        &self.type_vote
    }

    field uid() -> &str {
        &self.uid
    }

    field ventilation_votes() -> &OrganeVote {
        &self.ventilation_votes
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Scrutins {
    #[serde(rename = "scrutin")]
    pub scrutins: Vec<Scrutin>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ScrutinsJsonWrapper {
    pub scrutins: Scrutins,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct SortScrutin {
    code: String,
    libelle: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct SyntheseVote {
    annonce: String,
    decompte: DecompteVoix,
    #[serde(rename = "nbrSuffragesRequis")]
    nbr_suffrages_requis: String,
    #[serde(rename = "nombreVotants")]
    nombre_votants: String,
    #[serde(rename = "suffragesExprimes")]
    suffrages_exprimes: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct TypeVote {
    #[serde(rename = "codeTypeVote")]
    code_type_vote: String,
    #[serde(rename = "libelleTypeVote")]
    libelle_type_vote: String,
    #[serde(rename = "typeMajorite")]
    type_majorite: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Votant {
    #[serde(rename = "acteurRef")]
    acteur_ref: String,
    #[serde(rename = "mandatRef")]
    mandat_ref: String,
    #[serde(
        default,
        deserialize_with = "str_to_option_bool",
        rename = "parDelegation"
    )]
    par_delegation: Option<bool>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct VotantAvecMotivationVote {
    #[serde(rename = "acteurRef")]
    acteur_ref: String,
    #[serde(rename = "causePositionVote")]
    cause_position_vote: Option<CausePositionVote>,
    #[serde(rename = "mandatRef")]
    mandat_ref: String,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct VoteGroupe {
    #[serde(rename = "decompteNominatif")]
    decompte_nominatif: DecompteNominatif,
    #[serde(rename = "decompteVoix")]
    decompte_voix: DecompteVoix,
    #[serde(rename = "positionMajoritaire")]
    position_majoritaire: String,
}
