use chrono::{DateTime, Utc};
use juniper;
use std::collections::HashMap;

use crate::acteurs::Acteur;
use crate::acteurs_et_organes;
use crate::agendas::{self, Reunion};
use crate::amendements::{self, Amendement, TexteLeg};
use crate::dossiers_legislatifs::{self, DossierParlementaire};
use crate::organes::Organe;
use crate::scrutins::{self, Scrutin};
use crate::textes_legislatifs::Document;

pub struct Context {
    pub acteur_by_uid: HashMap<String, *const Acteur>,
    pub acteurs_et_organes_wrappers: Vec<acteurs_et_organes::ExportJsonWrapper>,
    pub agendas_wrappers: Vec<agendas::AgendaJsonWrapper>,
    pub amendement_by_uid: HashMap<String, *const Amendement>,
    pub amendements_wrappers: Vec<amendements::AmendementsJsonWrapper>,
    pub document_by_uid: HashMap<String, *const Document>,
    pub dossier_parlementaire_by_segment: HashMap<String, *const DossierParlementaire>,
    pub dossier_parlementaire_by_signet_senat: HashMap<String, *const DossierParlementaire>,
    pub dossier_parlementaire_by_uid: HashMap<String, *const DossierParlementaire>,
    pub dossiers_legislatifs_wrappers: Vec<dossiers_legislatifs::ExportJsonWrapper>,
    pub organe_by_uid: HashMap<String, *const Organe>,
    pub reunion_by_uid: HashMap<String, *const Reunion>,
    pub reunions_by_date: HashMap<String, Vec<*const Reunion>>,
    pub reunions_by_dossier_uid: HashMap<String, Vec<*const Reunion>>,
    pub scrutin_by_uid: HashMap<String, *const Scrutin>,
    pub scrutins_wrappers: Vec<scrutins::ScrutinsJsonWrapper>,
    pub statut: Statut,
    pub texte_leg_by_uid: HashMap<String, *const TexteLeg>,
}

impl Context {
    pub fn get_acteur_at_uid(&self, uid: &str) -> Option<&Acteur> {
        self.acteur_by_uid
            .get(&uid.to_string())
            .map(|acteur| unsafe { &*(*acteur) })
    }

    pub fn get_amendement_at_uid(&self, uid: &str) -> Option<&Amendement> {
        self.amendement_by_uid
            .get(&uid.to_string())
            .map(|amendement| unsafe { &*(*amendement) })
    }

    pub fn get_document_at_uid(&self, uid: &str) -> Option<&Document> {
        self.document_by_uid
            .get(&uid.to_string())
            .map(|document| unsafe { &*(*document) })
    }

    pub fn get_dossier_parlementaire_at_segment(
        &self,
        segment: &str,
    ) -> Option<&DossierParlementaire> {
        self.dossier_parlementaire_by_segment
            .get(&segment.to_string())
            .map(|dossier| unsafe { &*(*dossier) })
    }

    pub fn get_dossier_parlementaire_at_signet_senat(
        &self,
        signet_senat: &str,
    ) -> Option<&DossierParlementaire> {
        self.dossier_parlementaire_by_signet_senat
            .get(&signet_senat.to_string())
            .map(|dossier| unsafe { &*(*dossier) })
    }

    pub fn get_dossier_parlementaire_at_uid(&self, uid: &str) -> Option<&DossierParlementaire> {
        self.dossier_parlementaire_by_uid
            .get(&uid.to_string())
            .map(|dossier| unsafe { &*(*dossier) })
    }

    pub fn get_organe_at_uid(&self, uid: &str) -> Option<&Organe> {
        self.organe_by_uid
            .get(&uid.to_string())
            .map(|organe| unsafe { &*(*organe) })
    }

    pub fn get_reunion_at_uid(&self, uid: &str) -> Option<&Reunion> {
        self.reunion_by_uid
            .get(&uid.to_string())
            .map(|reunion| unsafe { &*(*reunion) })
    }

    pub fn get_reunions_at_date(&self, date: &str) -> Option<&Vec<&Reunion>> {
        self.reunions_by_date
            .get(&date.to_string())
            .map(|reunions| unsafe {
                &*(reunions as *const Vec<*const agendas::Reunion> as *const Vec<&agendas::Reunion>)
            })
    }

    pub fn get_reunions_at_dossier_uid(&self, id: &str) -> Option<&Vec<&Reunion>> {
        self.reunions_by_dossier_uid
            .get(&id.to_string())
            .map(|reunions| unsafe {
                &*(reunions as *const Vec<*const agendas::Reunion> as *const Vec<&agendas::Reunion>)
            })
    }

    pub fn get_scrutin_at_uid(&self, uid: &str) -> Option<&Scrutin> {
        self.scrutin_by_uid
            .get(&uid.to_string())
            .map(|scrutin| unsafe { &*(*scrutin) })
    }

    pub fn get_texte_leg_at_uid(&self, uid: &str) -> Option<&TexteLeg> {
        self.texte_leg_by_uid
            .get(&uid.to_string())
            .map(|texte_leg| unsafe { &*(*texte_leg) })
    }
}

unsafe impl Send for Context {}
unsafe impl Sync for Context {}
impl juniper::Context for Context {}

#[derive(Debug, Deserialize, GraphQLObject)]
pub struct Statut {
    pub date_mise_a_jour_donnees: DateTime<Utc>,
}
