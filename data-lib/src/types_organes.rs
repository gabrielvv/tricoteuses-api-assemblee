#[derive(Clone, Debug, GraphQLEnum)]
pub enum CodeFonctionOrgane {
    AppartenancePolitique,
    AutreFonction,
    FonctionAssembleeNationale,
    FonctionLieeMandatDepute,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum, PartialEq)]
pub enum CodeTypeOrgane {
    #[graphql(name = "API")]
    #[serde(rename = "API")]
    AssembleeParlementaireInternationale,
    #[graphql(name = "ASSEMBLEE")]
    #[serde(rename = "ASSEMBLEE")]
    Assemblee,
    #[graphql(name = "CJR")]
    #[serde(rename = "CJR")]
    CourJusticeRepublique,
    #[graphql(name = "CMP")]
    #[serde(rename = "CMP")]
    CommissionMixteParitaire,
    #[graphql(name = "CNPE")]
    #[serde(rename = "CNPE")] // TODO: What does it mean?
    CommissionEnquete,
    #[graphql(name = "CNPS")]
    #[serde(rename = "CNPS")] // TODO: What does it mean?
    CommissionSpeciale,
    #[graphql(name = "COMNL")]
    #[serde(rename = "COMNL")] // TODO: What does it mean?
    CommissionSpecialeAutre,
    #[graphql(name = "COMPER")]
    #[serde(rename = "COMPER")]
    CommissionPermanente,
    #[graphql(name = "COMSENAT")]
    #[serde(rename = "COMSENAT")]
    CommissionSenatoriale,
    #[graphql(name = "COMSPSENAT")]
    #[serde(rename = "COMSPSENAT")]
    CommissionSpecialeSenatoriale,
    #[graphql(name = "CONFPT")]
    #[serde(rename = "CONFPT")]
    ConferencePresidents,
    #[graphql(name = "CONSTITU")]
    #[serde(rename = "CONSTITU")]
    ConseilConstitutionnel,
    #[graphql(name = "DELEG")]
    #[serde(rename = "DELEG")]
    Delegation,
    #[graphql(name = "DELEGBUREAU")]
    #[serde(rename = "DELEGBUREAU")]
    DelegationBureau,
    #[graphql(name = "DELEGSENAT")]
    #[serde(rename = "DELEGSENAT")]
    DelegationSenatoriale,
    #[graphql(name = "GA")]
    #[serde(rename = "GA")]
    GroupeAmitie,
    #[graphql(name = "GE")]
    #[serde(rename = "GE")]
    GroupeEtudes,
    #[graphql(name = "GEVI")]
    #[serde(rename = "GEVI")]
    GroupeEtudesVocationInternationale,
    #[graphql(name = "GOUVERNEMENT")]
    #[serde(rename = "GOUVERNEMENT")]
    Gouvernement,
    #[graphql(name = "GP")]
    #[serde(rename = "GP")]
    GroupePolitique,
    #[graphql(name = "GROUPESENAT")]
    #[serde(rename = "GROUPESENAT")]
    GroupeSenatorial,
    #[graphql(name = "HCJ")]
    #[serde(rename = "HCJ")]
    HauteCour,
    #[graphql(name = "MINISTERE")]
    #[serde(rename = "MINISTERE")]
    Ministere,
    #[graphql(name = "MISINFO")]
    #[serde(rename = "MISINFO")]
    MissionInformation,
    #[graphql(name = "MISINFOCOM")]
    #[serde(rename = "MISINFOCOM")]
    MissionInformationCommune,
    #[graphql(name = "MISINFOPRE")]
    #[serde(rename = "MISINFOPRE")]
    MissionInformationPrealable,
    #[graphql(name = "OFFPAR")]
    #[serde(rename = "OFFPAR")]
    OfficeParlementaire,
    #[graphql(name = "ORGAINT")]
    #[serde(rename = "ORGAINT")]
    OrganismeInternational,
    #[graphql(name = "ORGEXTPARL")]
    #[serde(rename = "ORGEXTPARL")]
    OrganismeExtraParlementaire,
    #[graphql(name = "PARPOL")]
    #[serde(rename = "PARPOL")]
    PartiPolitique,
    #[graphql(name = "PRESREP")]
    #[serde(rename = "PRESREP")]
    PresidenceRepublique,
    #[graphql(name = "SENAT")]
    #[serde(rename = "SENAT")]
    Senat,
}

#[derive(Clone, Debug, GraphQLObject)]
pub struct FonctionOrgane {
    code: CodeFonctionOrgane,
    libelle: &'static str,
    libelle_pluriel: &'static str,
    types_organes: &'static [&'static TypeOrgane],
}

#[derive(Clone, Debug, GraphQLObject)]
pub struct TypeOrgane {
    code: CodeTypeOrgane,
    libelle: &'static str,
    libelle_pluriel: &'static str,
}

pub const FONCTIONS_ORGANES: &[&FonctionOrgane] = &[
    &FonctionOrgane {
        code: CodeFonctionOrgane::AppartenancePolitique,
        libelle: "Appartenance politique",
        libelle_pluriel: "Appartenance politique",
        types_organes: &[
            &TypeOrgane {
                code: CodeTypeOrgane::GroupePolitique,
                libelle: "Groupe politique",
                libelle_pluriel: "Groupes politiques",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::PartiPolitique,
                libelle: "Rattachement au titre du financement de la vie politique",
                libelle_pluriel: "Rattachements au titre du financement de la vie politique",
            },
        ],
    },
    &FonctionOrgane {
        code: CodeFonctionOrgane::FonctionAssembleeNationale,
        libelle: "Fonction à l'Assemblée nationale",
        libelle_pluriel: "Fonctions à l'Assemblée nationale",
        types_organes: &[
            &TypeOrgane {
                code: CodeTypeOrgane::Assemblee,
                libelle: "Mandat",
                libelle_pluriel: "Mandats",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::CommissionMixteParitaire,
                libelle: "Commission mixte paritaire",
                libelle_pluriel: "Commissions mixtes paritaires",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::CommissionEnquete,
                libelle: "Commission d'enquête",
                libelle_pluriel: "Commissions d'enquête",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::CommissionSpeciale,
                libelle: "Commission spéciale",
                libelle_pluriel: "Commissions spéciales",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::CommissionSpecialeAutre,
                libelle: "Commission spéciale (autre)",
                libelle_pluriel: "Commissions spéciales (autres)",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::CommissionPermanente,
                libelle: "Commission permanente",
                libelle_pluriel: "Commissions permanentes",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::ConferencePresidents,
                libelle: "Conférence des présidents",
                libelle_pluriel: "Conférence des présidents",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::Delegation,
                libelle: "Délégation",
                libelle_pluriel: "Délégations",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::DelegationBureau,
                libelle: "Délégation du Bureau",
                libelle_pluriel: "Délégations du Bureau",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::GroupeAmitie,
                libelle: "Groupe d'amitié",
                libelle_pluriel: "Groupes d'amitié",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::GroupeEtudes,
                libelle: "Groupe d'études",
                libelle_pluriel: "Groupes d'études",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::GroupeEtudesVocationInternationale,
                libelle: "Groupe d'études à vocation internationale",
                libelle_pluriel: "Groupes d'études à vocation internationale",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::MissionInformation,
                libelle: "Mission d'information",
                libelle_pluriel: "Missions d'information",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::MissionInformationCommune,
                libelle: "Mission d'information commune",
                libelle_pluriel: "Missions d'information commune",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::MissionInformationPrealable,
                libelle: "Mission d'information prélable",
                libelle_pluriel: "Missions d'information prélable",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::OfficeParlementaire,
                libelle: "Office parlementaire",
                libelle_pluriel: "Offices parlementaires",
            },
        ],
    },
    &FonctionOrgane {
        code: CodeFonctionOrgane::FonctionLieeMandatDepute,
        libelle: "Fonction liée au mandat de député",
        libelle_pluriel: "Fonctions liées au mandat de député",
        types_organes: &[
            &TypeOrgane {
                code: CodeTypeOrgane::AssembleeParlementaireInternationale,
                libelle: "Instance parlementaire internationale",
                libelle_pluriel: "Instances parlementaires internationales",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::CourJusticeRepublique,
                libelle: "Instance judiciaire",
                libelle_pluriel: "Instances judiciaires",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::OrganismeInternational,
                libelle: "Instance internationale",
                libelle_pluriel: "Instances internationales",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::OrganismeExtraParlementaire,
                libelle: "Organisme extra-parlementaire",
                libelle_pluriel: "Organismes extra-parlementaires",
            },
        ],
    },
    &FonctionOrgane {
        code: CodeFonctionOrgane::AutreFonction,
        libelle: "Autre fonction",
        libelle_pluriel: "Autres fonctions",
        types_organes: &[
            &TypeOrgane {
                code: CodeTypeOrgane::CommissionSenatoriale,
                libelle: "Commission sénatoriale",
                libelle_pluriel: "Commissions sénatoriales",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::CommissionSpecialeSenatoriale,
                libelle: "Commission spéciale sénatoriale",
                libelle_pluriel: "Commissions spéciales sénatoriales",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::ConseilConstitutionnel,
                libelle: "Conseil constitutionnel",
                libelle_pluriel: "Conseil constitutionnel",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::DelegationSenatoriale,
                libelle: "Délégation sénatoriale",
                libelle_pluriel: "Délégations sénatoriales",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::Gouvernement,
                libelle: "Gouvernement",
                libelle_pluriel: "Gouvernement",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::GroupeSenatorial,
                libelle: "Groupe politique du Sénat",
                libelle_pluriel: "Groupes politiques du Sénat",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::HauteCour,
                libelle: "Haute Cour",
                libelle_pluriel: "Haute Cour",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::Ministere,
                libelle: "Ministère",
                libelle_pluriel: "Ministères",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::PresidenceRepublique,
                libelle: "Présidence de la République",
                libelle_pluriel: "Présidence de la République",
            },
            &TypeOrgane {
                code: CodeTypeOrgane::Senat,
                libelle: "Sénat",
                libelle_pluriel: "Sénat",
            },
        ],
    },
];
