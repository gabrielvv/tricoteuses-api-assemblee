use crate::acteurs::Acteur;
use crate::commun::{EmptyChoice, XmlNamespace};
use crate::contexts::Context;
use crate::organes::Organe;
use crate::serde_utils::{map_to_vec, str_to_bool, str_to_option_bool, str_to_vec};
use crate::textes_legislatifs::Document;

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Alinea {
    #[serde(rename = "alineaDesignation")]
    pub alinea_designation: Option<String>,
    #[serde(rename = "avant_A_Apres")]
    pub avant_a_apres: Option<String>,
    #[serde(rename = "numero")]
    pub numero: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Amendement {
    #[serde(rename = "amendementParent")]
    pub amendement_parent: Option<String>,
    #[serde(rename = "article99")]
    pub article_99: Option<String>,
    #[serde(rename = "cardinaliteAmdtMultiples")]
    pub cardinalite_amd_multiples: Option<String>,
    #[serde(rename = "corps")]
    pub corps: Option<Corps>,
    #[serde(rename = "dateDepot")]
    pub date_depot: Option<String>,
    #[serde(rename = "dateDistribution")]
    pub date_distribution: Option<String>,
    #[serde(rename = "etapeTexte")]
    pub etape_texte: Option<String>,
    #[serde(rename = "etat")]
    pub etat: Option<String>,
    #[serde(rename = "identifiant")]
    pub identifiant: Option<Identifiant>,
    #[serde(rename = "loiReference")]
    pub loi_reference: Option<LoiReference>,
    #[serde(rename = "numeroLong")]
    pub numero_long: Option<String>,
    #[serde(rename = "pointeurFragmentTexte")]
    pub pointeur_fragment_texte: Option<PointeurFragmentTexte>,
    #[serde(rename = "representations")]
    pub representations: Option<Representations>,
    #[serde(rename = "seanceDiscussion")]
    pub seance_discussion: Option<String>,
    #[serde(rename = "signataires")]
    pub signataires: Option<Signataires>,
    #[serde(rename = "sort")]
    pub sort: Option<SortAmendement>,
    #[serde(rename = "triAmendement")]
    pub tri_amendement: Option<String>,
    #[serde(rename = "uid")]
    pub uid: String,
}

graphql_object!(Amendement: Context |&self| {
    field amendement_parent() -> Option<&str> {
        match &self.amendement_parent {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field article_99() -> Option<&str> {
        match &self.article_99 {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field cardinalite_amd_multiples() -> Option<&str> {
        match &self.cardinalite_amd_multiples {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field corps() -> &Option<Corps> {
        &self.corps
    }

    field date_depot() -> Option<&str> {
        match &self.date_depot {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field date_distribution() -> Option<&str> {
        match &self.date_distribution {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field etape_texte() -> Option<&str> {
        match &self.etape_texte {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field etat() -> Option<&str> {
        match &self.etat {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field identifiant() -> &Option<Identifiant> {
        &self.identifiant
    }

    field loi_reference() -> &Option<LoiReference> {
        &self.loi_reference
    }

    field numero_long() -> Option<&str> {
        match &self.numero_long {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field pointeur_fragment_texte() -> &Option<PointeurFragmentTexte> {
        &self.pointeur_fragment_texte
    }

    field representations() -> Vec<&Representation> {
        match self.representations {
            None => vec![],
            Some(ref representations) => {
                representations.representations
                    .iter()
                    .collect()
            },
        }
    }

    field seance_discussion() -> Option<&str> {
        match &self.seance_discussion {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field signataires() -> &Option<Signataires> {
        &self.signataires
    }

    field sort() -> &Option<SortAmendement> {
        &self.sort
    }

    field tri_amendement() -> Option<&str> {
        match &self.tri_amendement {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field uid() -> &str {
        &self.uid
    }
});

#[derive(Debug, Deserialize)]
// See field codeLoi below to know why option "deny_unknown_fields" is commented out.
// #[serde(deny_unknown_fields)]
pub struct Amendements {
    #[serde(rename = "acteurRef")]
    pub acteur_ref: Option<String>,
    #[serde(deserialize_with = "map_to_vec", rename = "amendement")]
    pub amendements: Vec<Amendement>,
    #[serde(rename = "annexeExposeSommaire")]
    pub annexe_expose_sommaire: Option<EmptyChoice>,
    #[serde(rename = "article99")]
    pub article_99: Option<String>,
    #[serde(rename = "articleAdditionnel")]
    pub article_additionnel: Option<String>,
    #[serde(default, deserialize_with = "str_to_option_bool")]
    pub canonique: Option<bool>,
    // codeLoi can be null, [null, null], so a conversion from Option<str> to Vec<str> is needed.
    // In the mean time, don't deserialize this field & remove "deny_unknown_fields" above.
    // #[serde(rename = "codeLoi")]
    // pub code_loi: Vec<String>,
    #[serde(rename = "dateDepot")]
    pub date_depot: Option<String>,
    #[serde(rename = "dateDispoRepresentation")]
    pub date_dispo_representation: Option<String>,
    #[serde(default, deserialize_with = "str_to_vec", rename = "dateDistribution")]
    pub date_distribution: Vec<String>,
    #[serde(rename = "divisionCodeLoi")]
    pub division_code_loi: Option<EmptyChoice>,
    #[serde(rename = "documentURI")]
    pub document_uri: Option<String>,
    pub libelle: Option<String>,
    #[serde(rename = "lignesCredits")]
    pub lignes_credits: Option<EmptyChoice>,
    #[serde(default, deserialize_with = "map_to_vec", rename = "loiReference")]
    pub loi_reference: Vec<LoiReference>,
    #[serde(rename = "montantNegatif")]
    pub montant_negatif: Option<String>,
    #[serde(rename = "montantPositif")]
    pub montant_positif: Option<String>,
    pub nom: Option<String>,
    #[serde(default, deserialize_with = "str_to_option_bool")]
    pub officielle: Option<bool>,
    pub offset: Option<EmptyChoice>,
    #[serde(rename = "repSource")]
    pub rep_source: Option<String>,
    #[serde(rename = "seanceDiscussion")]
    pub seance_discussion: Option<String>,
    #[serde(rename = "sort")]
    pub sort: Option<SortAmendement>,
    #[serde(rename = "subType")]
    pub sub_type: Option<String>,
    #[serde(rename = "#text")]
    pub text: Option<String>,
    #[serde(default, deserialize_with = "str_to_option_bool")]
    pub transcription: Option<bool>,
    #[serde(default, deserialize_with = "map_to_vec", rename = "typeMime")]
    pub type_mime: Vec<TypeMime>,
    #[serde(default, deserialize_with = "str_to_option_bool")]
    pub verbatim: Option<bool>,
}

graphql_object!(Amendements: Context |&self| {
    field acteur(&executor) -> Option<&Acteur> {
        match &self.acteur_ref {
            None => None,
            Some(ref acteur_ref) => {
                let context = &executor.context();
                context.get_acteur_at_uid(acteur_ref)
            },
        }
    }

    field acteur_ref() -> Option<&str> {
        match &self.acteur_ref {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field amendements() -> &Vec<Amendement> {
        &self.amendements
    }

    field article_99() -> Option<&str> {
        match &self.article_99 {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field article_additionnel() -> Option<&str> {
        match &self.article_additionnel {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field canonique() -> Option<bool> {
        self.canonique
    }

    // field code_loi() -> &Vec<String> {
    //     &self.code_loi
    // }

    field date_depot() -> Option<&str> {
        match &self.date_depot {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field date_dispo_representation() -> Option<&str> {
        match &self.date_dispo_representation {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field date_distribution() -> &Vec<String> {
        &self.date_distribution
    }

    field document_uri() -> Option<&str> {
        match &self.document_uri {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field libelle() -> Option<&str> {
        match &self.libelle {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field loi_reference() -> &Vec<LoiReference> {
        &self.loi_reference
    }

    field montant_negatif() -> Option<&str> {
        match &self.montant_negatif {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field montant_positif() -> Option<&str> {
        match &self.montant_positif {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field nom() -> Option<&str> {
        match &self.nom {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field officielle() -> Option<bool> {
        self.officielle
    }

    field rep_source() -> Option<&str> {
        match &self.rep_source {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field seance_discussion() -> Option<&str> {
        match &self.seance_discussion {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field sort() -> &Option<SortAmendement> {
        &self.sort
    }

    field sub_type() -> Option<&str> {
        match &self.sub_type {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field text() -> Option<&str> {
        match &self.text {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field transcription() -> Option<bool> {
        self.transcription
    }

    field type_mime() -> &Vec<TypeMime> {
        &self.type_mime
    }

    field verbatim() -> Option<bool> {
        self.verbatim
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct AmendementsJsonWrapper {
    #[serde(rename = "textesEtAmendements")]
    pub textes_et_amendements: Option<TextesEtAmendements>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Auteur {
    #[serde(rename = "acteurRef")]
    pub acteur_ref: Option<String>,
    #[serde(rename = "groupePolitiqueRef")]
    pub groupe_politique_ref: Option<String>,
    #[serde(rename = "organeRef")]
    pub organe_ref: Option<String>,
    #[serde(rename = "typeAuteur")]
    pub type_auteur: TypeAuteur,
}

graphql_object!(Auteur: Context |&self| {
    field acteur(&executor) -> Option<&Acteur> {
        match &self.acteur_ref {
            None => None,
            Some(ref acteur_ref) => {
                let context = &executor.context();
                context.get_acteur_at_uid(acteur_ref)
            },
        }
    }

    field acteur_ref() -> Option<&str> {
        match &self.acteur_ref {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field groupe_politique(&executor) -> Option<&Organe> {
        match &self.groupe_politique_ref {
            None => None,
            Some(ref groupe_politique_ref) => {
                let context = &executor.context();
                context.get_organe_at_uid(groupe_politique_ref)
            },
        }
    }

    field groupe_politique_ref() -> Option<&str> {
        match &self.groupe_politique_ref {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field organe(&executor) -> Option<&Organe> {
        match &self.organe_ref {
            None => None,
            Some(ref organe_ref) => {
                let context = &executor.context();
                context.get_organe_at_uid(organe_ref)
            },
        }
    }

    field organe_ref() -> Option<&str> {
        match &self.organe_ref {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field type_auteur() -> TypeAuteur {
        self.type_auteur
    }
});

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Contenu {
    #[serde(rename = "documentURI")]
    pub document_uri: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Corps {
    #[serde(rename = "annexeExposeSommaire")]
    pub annexe_expose_sommaire: Option<EmptyChoice>,
    pub dispositif: Option<String>,
    #[serde(rename = "cartoucheDelaiDepotDepasse")]
    pub cartouche_delai_depot_depasse: Option<String>,
    #[serde(rename = "dispositifAmdtCredit")]
    pub dispositif_amdt_credit: Option<ListeProgrammes>,
    #[serde(rename = "exposeSommaire")]
    pub expose_sommaire: Option<String>,
    #[serde(rename = "totalAE")]
    pub total_ae: Option<TotalMontants>,
    #[serde(rename = "totalCP")]
    pub total_cp: Option<TotalMontants>,
}

graphql_object!(Corps: Context |&self| {
    field dispositif() -> Option<&str> {
        match &self.dispositif {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field cartouche_delai_depot_depasse() -> Option<&str> {
        match &self.cartouche_delai_depot_depasse {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field dispositif_amdt_credit() -> &Option<ListeProgrammes> {
        &self.dispositif_amdt_credit
    }

    field expose_sommaire() -> Option<&str> {
        match &self.expose_sommaire {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field total_ae() -> &Option<TotalMontants> {
        &self.total_ae
    }

    field total_cp() -> &Option<TotalMontants> {
        &self.total_cp
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Cosignataires {
    #[serde(deserialize_with = "str_to_vec", rename = "acteurRef")]
    pub acteurs_refs: Vec<String>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Division {
    #[serde(rename = "articleAdditionnel")]
    pub article_additionnel: Option<String>,
    #[serde(rename = "articleDesignationCourte")]
    pub article_designation_courte: Option<String>,
    #[serde(rename = "avant_A_Apres")]
    pub avant_a_apres: Option<String>,
    #[serde(rename = "chapitreAdditionnel")]
    pub chapitre_additionnel: Option<String>,
    #[serde(rename = "divisionRattachee")]
    pub division_rattachee: Option<String>,
    #[serde(rename = "titre")]
    pub titre: Option<String>,
    #[serde(rename = "type")]
    type_texte: Option<String>,
    #[serde(rename = "urlDivisionTexteVise")]
    url_division_texte_vise: Option<String>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Identifiant {
    #[serde(rename = "legislature")]
    pub legislature: Option<String>,
    #[serde(rename = "numero")]
    pub numero: Option<String>,
    #[serde(rename = "numRect")]
    pub num_rect: Option<String>,
    #[serde(rename = "saisine")]
    pub saisine: Option<Saisine>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct LigneCredit {
    #[serde(rename = "AE")]
    pub ae: Option<Montants>,
    #[serde(rename = "CP")]
    pub cp: Option<Montants>,
    #[serde(rename = "id")]
    pub id: Option<String>,
    pub libelle: Option<String>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct LignesCredits {
    #[serde(deserialize_with = "map_to_vec", rename = "ligneCredit")]
    pub lignes_credits: Vec<LigneCredit>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ListeProgrammes {
    #[serde(rename = "listeProgrammes")]
    pub liste_programmes: Option<Programmes>,
    #[serde(rename = "totalAE")]
    pub total_ae: Option<TotalMontants>,
    #[serde(rename = "totalCP")]
    pub total_cp: Option<TotalMontants>,
}

graphql_object!(ListeProgrammes: Context |&self| {
    field liste_programmes() -> Vec<&Programme> {
        match self.liste_programmes {
            None => vec![],
            Some(ref liste_programmes) => {
                liste_programmes.programmes
                    .iter()
                    .collect()
            },
        }
    }

    field total_ae() -> &Option<TotalMontants> {
        &self.total_ae
    }

    field total_cp() -> &Option<TotalMontants> {
        &self.total_cp
    }
});

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct LoiReference {
    #[serde(rename = "codeLoi")]
    pub code_loi: Option<String>,
    #[serde(rename = "divisionCodeLoi")]
    pub division_code_loi: Option<String>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct MissionVisee {
    #[serde(rename = "codeMissionPLF")]
    pub code_mission_plf: Option<String>,
    #[serde(rename = "idMissionAN")]
    pub id_mission_an: Option<String>,
    #[serde(rename = "libelleMission")]
    pub libelle_mission: Option<String>,
    #[serde(rename = "libelleMissionPLF")]
    pub libelle_mission_plf: Option<String>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Montants {
    #[serde(rename = "montantNegatif")]
    pub montant_negatif: Option<String>,
    #[serde(rename = "montantPositif")]
    pub montant_positif: Option<String>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct PointeurFragmentTexte {
    #[serde(rename = "alinea")]
    pub alinea: Option<Alinea>,
    #[serde(rename = "division")]
    pub division: Option<Division>,
    #[serde(rename = "missionVisee")]
    pub mission_visee: Option<MissionVisee>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Programme {
    pub action: Option<String>,
    #[serde(rename = "AE")]
    pub ae: Option<Montants>,
    #[serde(rename = "CP")]
    pub cp: Option<Montants>,
    pub id: Option<String>,
    pub libelle: Option<String>,
    #[serde(rename = "lignesCredits")]
    pub lignes_credits: Option<LignesCredits>,
}

graphql_object!(Programme: Context |&self| {
    field action() -> Option<&str> {
        match &self.action {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field ae() -> &Option<Montants> {
        &self.ae
    }

    field cp() -> &Option<Montants> {
        &self.cp
    }

    field id() -> Option<&str> {
        match &self.id {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field libelle() -> Option<&str> {
        match &self.libelle {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field lignes_credits() -> Vec<&LigneCredit> {
        match self.lignes_credits {
            None => vec![],
            Some(ref lignes_credits) => {
                lignes_credits.lignes_credits
                    .iter()
                    .collect()
            },
        }
    }

});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Programmes {
    #[serde(deserialize_with = "map_to_vec", rename = "programme")]
    pub programmes: Vec<Programme>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Representation {
    pub contenu: Option<Contenu>,
    #[serde(rename = "dateDispoRepresentation")]
    pub date_dispo_representation: Option<String>,
    pub nom: Option<String>,
    pub offset: Option<EmptyChoice>,
    #[serde(rename = "repSource")]
    pub rep_source: Option<String>,
    #[serde(rename = "statutRepresentation")]
    pub statut_representation: Option<StatutRepresentation>,
    #[serde(rename = "typeMime")]
    pub type_mime: TypeMime,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Representations {
    #[serde(deserialize_with = "map_to_vec", rename = "representation")]
    pub representations: Vec<Representation>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct Saisine {
    #[serde(rename = "mentionSecondeDeliberation")]
    pub mention_seconde_deliberation: Option<String>,
    #[serde(rename = "numeroPartiePLF")]
    pub numero_partie_plf: Option<String>,
    #[serde(rename = "organeExamen")]
    pub organe_examen: Option<String>,
    #[serde(rename = "refTexteLegislatif")]
    pub ref_texte_legislatif: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Signataires {
    #[serde(rename = "auteur")]
    pub auteur: Option<Auteur>,
    #[serde(rename = "cosignataires")]
    pub cosignataires: Option<Cosignataires>,
    #[serde(rename = "texteAffichable")]
    pub texte_affichable: Option<String>,
}

graphql_object!(Signataires: Context |&self| {
    field auteur() -> &Option<Auteur> {
        &self.auteur
    }

    field cosignataires(&executor) -> Vec<&Acteur> {
        match self.cosignataires {
            None => vec![],
            Some(ref cosignataires) => {
                let context = &executor.context();
                cosignataires.acteurs_refs
                    .iter()
                    .filter_map(|acteur_ref| context.get_acteur_at_uid(acteur_ref))
                    .collect()
            },
        }
    }

    field cosignataires_refs() -> Vec<&str> {
        match self.cosignataires {
            None => vec![],
            Some(ref cosignataires) => {
                cosignataires.acteurs_refs
                    .iter()
                    .map(|acteur_ref| acteur_ref.as_str())
                    .collect()
            },
        }
    }

    field texte_affichable() -> Option<&str> {
        match &self.texte_affichable {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }
});

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct SortAmendement {
    #[serde(rename = "dateSaisie")]
    pub date_saisie: Option<String>,
    #[serde(rename = "sortEnSeance")]
    pub sort_en_seance: Option<String>,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct StatutRepresentation {
    #[serde(deserialize_with = "str_to_bool")]
    pub canonique: bool,
    #[serde(deserialize_with = "str_to_bool")]
    pub enregistrement: bool,
    #[serde(deserialize_with = "str_to_bool")]
    pub officielle: bool,
    #[serde(deserialize_with = "str_to_bool")]
    pub transcription: bool,
    #[serde(deserialize_with = "str_to_bool")]
    pub verbatim: bool,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct TextesEtAmendements {
    #[serde(rename = "texteleg")]
    pub texte_leg: Vec<TexteLeg>,
}

graphql_object!(TextesEtAmendements: Context |&self| {
    field texte_leg() -> &Vec<TexteLeg> {
        &self.texte_leg
    }
});

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct TexteLeg {
    #[serde(rename = "@xmlns:xsi")]
    pub xml_xsi: Option<XmlNamespace>,
    #[serde(rename = "amendements")]
    pub amendements: Option<Amendements>,
    #[serde(rename = "refTexteLegislatif")]
    pub ref_texte_legislatif: Option<String>,
}

graphql_object!(TexteLeg: Context |&self| {
    field amendements() -> &Option<Amendements> {
        &self.amendements
    }

    field ref_texte_legislatif() -> Option<&str> {
        match &self.ref_texte_legislatif {
            None => None,
            Some(ref s) => Some(s.as_str()),
        }
    }

    field texte_legislatif(&executor) -> Vec<&Document> {
        match &self.ref_texte_legislatif {
            None => vec![],
            Some(ref ref_texte_legislatif) => {
                let context = &executor.context();
                match context.get_document_at_uid(ref_texte_legislatif) {
                    None => vec![],
                    Some(ref document) => {
                        // Generate a flat vector from document and its sub-documents (aka divisions),
                        // because a tree is not adapted to GraphQL.
                        document.flatten_document()
                    },
                }
            },
        }
    }
});

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct TotalMontants {
    #[serde(rename = "montantNegatif")]
    pub montant_negatif: Option<String>,
    #[serde(rename = "montantPositif")]
    pub montant_positif: Option<String>,
    pub solde: Option<String>,
}

#[derive(Clone, Copy, Debug, Deserialize, GraphQLEnum)]
pub enum TypeAuteur {
    #[graphql(name = "Depute")]
    Depute,
    #[graphql(name = "Gouvernement")]
    Gouvernement,
    #[graphql(name = "Rapporteur")]
    Rapporteur,
}

#[derive(Debug, Deserialize, GraphQLObject)]
#[serde(deny_unknown_fields)]
pub struct TypeMime {
    #[serde(rename = "type")]
    mime_type: Option<String>,
    #[serde(rename = "subType")]
    pub sub_type: Option<String>,
}
