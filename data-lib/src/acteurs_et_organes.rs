use crate::acteurs::Acteurs;
use crate::commun::XmlNamespace;
use crate::organes::Organes;

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Export {
    pub acteurs: Acteurs,
    pub organes: Organes,
    #[serde(rename = "@xmlns:xsi")]
    xml_xsi: XmlNamespace,
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ExportJsonWrapper {
    pub export: Export,
}
