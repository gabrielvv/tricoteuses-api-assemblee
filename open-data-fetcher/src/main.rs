extern crate clap;
extern crate rayon;
extern crate tricoteuses_api_assemblee_config as config;

use clap::{App, Arg};
use config::Verbosity;
use rayon::prelude::*;
use std::fs::{self, rename, File};
use std::path::Path;
use std::process::Command;

fn main() {
    let matches = App::new("Tricoteuses API Assemblée")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Emmanuel Raviart <emmanuel@raviart.com>")
        .about("GraphQL API to access open data of French Assemblée nationale")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let config_file_path = Path::new(matches.value_of("config").unwrap_or("../Config.toml"));
    let config = config::load(config_file_path);

    let verbosity = match matches.occurrences_of("v") {
        0 => Verbosity::Verbosity0,
        1 => Verbosity::Verbosity1,
        2 | _ => Verbosity::Verbosity2,
    };

    let config_dir = config_file_path.parent().unwrap();
    let data_dir = config_dir.join(config.data.dir);
    if !data_dir.exists() {
        fs::create_dir(&data_dir).expect("Creation of data directory failed");
    }

    let datasets: Vec<&config::Dataset> = config
        .acteurs_et_organes
        .iter()
        .chain(config.agendas.iter())
        .chain(config.amendements.iter())
        .chain(config.dossiers_legislatifs.iter())
        .chain(config.scrutins.iter())
        .collect();
    datasets.par_iter().for_each(|dataset| {
        if verbosity != Verbosity::Verbosity0 {
            println!("Loading \"{}\"...", dataset.title);
        }

        let output = Command::new("wget")
            .current_dir(&data_dir)
            .arg("-q")
            .arg(&dataset.url)
            .output()
            .expect("Wget failed");
        if !output.status.success() {
            println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
            println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
            panic!("Wget failed");
        }

        let json_filename = &dataset.filename;
        let zip_filename = format!("{}.zip", json_filename);
        let output = Command::new("unzip")
            .current_dir(&data_dir)
            .arg("-o")
            .arg("-q")
            .arg(&zip_filename)
            .output()
            .expect("Unzip failed");
        if !output.status.success() {
            println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
            println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
            panic!("Unzip failed");
        }

        let output = Command::new("rm")
            .current_dir(&data_dir)
            .arg(&zip_filename)
            .output()
            .expect("Rm failed");
        if !output.status.success() {
            println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
            println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
            panic!("Rm failed");
        }

        let temp_json_file_path = data_dir.join(format!("temp_{}", &json_filename));
        let temp_json_file = File::create(&temp_json_file_path).unwrap();
        let output = Command::new("jq")
            .current_dir(&data_dir)
            .arg(".")
            .arg(json_filename)
            .stdout(temp_json_file)
            .output()
            .expect("Jq failed");
        if !output.status.success() {
            println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
            panic!("Jq failed");
        }
        rename(&temp_json_file_path, data_dir.join(json_filename)).expect("Rename failed");

        if verbosity != Verbosity::Verbosity0 {
            println!("Loaded \"{}\".", dataset.title);
        }
    })
}
