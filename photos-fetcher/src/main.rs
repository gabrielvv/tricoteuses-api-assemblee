extern crate chrono;
extern crate clap;
extern crate rayon;
extern crate serde_json;
extern crate tricoteuses_api_assemblee_config as config;
extern crate tricoteuses_api_assemblee_data as data;

use clap::{App, Arg};
use config::Verbosity;
use data::{Acteur, CodeTypeOrgane, EnabledDatasets, Mandat, PhotoDepute, NO_DATASET};
use rayon::prelude::*;
use std::collections::HashMap;
use std::fs::{self, File};
use std::path::Path;
use std::process::Command;

fn main() {
    let matches = App::new("Tricoteuses API Assemblée")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Emmanuel Raviart <emmanuel@raviart.com>")
        .about("GraphQL API to access open data of French Assemblée nationale")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("no_download")
                .short("D")
                .long("no-download")
                .help("Reuse existing photos (don't download them)"),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let config_file_path = Path::new(matches.value_of("config").unwrap_or("../Config.toml"));
    let config = config::load(config_file_path);

    let verbosity = match matches.occurrences_of("v") {
        0 => Verbosity::Verbosity0,
        1 => Verbosity::Verbosity1,
        2 | _ => Verbosity::Verbosity2,
    };

    let config_dir = config_file_path.parent().unwrap();

    let context = data::load(
        &config,
        &config_dir,
        &EnabledDatasets {
            acteurs_et_organes: true,
            ..NO_DATASET
        },
        &verbosity,
    );

    let data_dir = config_dir.join(config.data.dir);
    let legislature = "15";
    let photos_dir = data_dir.join(format!("photos_deputes_{}", legislature));

    let mut deputes = Vec::<&Acteur>::new();
    for acteur in context
        .acteur_by_uid
        .values()
        .map(|acteur| unsafe { &*(*acteur) })
    {
        for mandat in acteur.mandats(Some(&chrono::Local::today().format("%Y-%m-%d").to_string())) {
            if let Mandat::MandatParlementaire(mandat_parlementaire) = mandat {
                if let Some(legislature_mandat) = &mandat_parlementaire.legislature {
                    if legislature_mandat == legislature
                        && mandat_parlementaire.type_organe == CodeTypeOrgane::Assemblee
                    {
                        deputes.push(acteur);
                        break; // Don't push the same deputé several times.
                    }
                }
            }
        }
    }
    deputes.sort_unstable_by(|depute1, depute2| depute1.uid.text.cmp(&depute2.uid.text));

    // Download photos.
    if !photos_dir.exists() {
        fs::create_dir(&photos_dir).expect("Creation of photos directory failed");
    }
    if !matches.is_present("no_download") {
        let missing_photo_file_path = Path::new("images/transparent_150x192.jpg");
        deputes.par_iter().for_each(|depute| {
            let ident = &depute.etat_civil.ident;
            let photo_filename = format!("{}.jpg", depute.uid().get(2..).unwrap());
            let photo_file_path = photos_dir.join(&photo_filename);
            let photo_temp_filename = format!("{}_temp.jpg", depute.uid().get(2..).unwrap());
            let photo_temp_file_path = photos_dir.join(&photo_temp_filename);
            let url_photo = format!(
                "http://www2.assemblee-nationale.fr/static/tribun/{}/photos/{}",
                legislature, photo_filename
            );
            if verbosity != Verbosity::Verbosity0 {
                println!(
                    "Loading photo {} for {} {}...",
                    url_photo, ident.prenom, ident.nom
                );
            }
            for retries in 0..3 {
                let output = Command::new("wget")
                    .current_dir(&photos_dir)
                    .arg("-q")
                    .arg("-O")
                    .arg(&photo_temp_filename)
                    .arg(&url_photo)
                    .output()
                    .expect("Wget failed");
                if output.status.success() {
                    fs::rename(&photo_temp_file_path, &photo_file_path)
                        .expect("Renaming of image failed");
                    return;
                }
                if retries >= 2 {
                    println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
                    println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
                    println!(
                        "Wget of {} ({} {}) failed",
                        url_photo, ident.prenom, ident.nom
                    );
                    if photo_file_path.exists() {
                        println!("  => Reusing existing image");
                    } else {
                        println!("  => Using blank image");
                        fs::copy(&missing_photo_file_path, &photo_file_path)
                            .expect("Copy of blank image failed");
                    }
                }
                if photo_temp_file_path.exists() {
                    fs::remove_file(&photo_temp_file_path).expect("Temporary image removal failed");
                }
            }
        });
    }

    // Resize photos to 150x192, because some haven't exactly this size.
    deputes.par_iter().for_each(|depute| {
        let numero_depute = depute.uid().get(2..).unwrap();
        let output = Command::new("gm")
            .current_dir(&photos_dir)
            .arg("convert")
            .arg("-resize")
            .arg("150x192!")
            .arg(format!("{}.jpg", numero_depute))
            .arg(format!("{}_150x192.jpg", numero_depute))
            .output()
            .expect("Gm photo resizing failed");
        if !output.status.success() {
            println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
            println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
            panic!("Gm photo resizing failed");
        }
    });

    // Create a mosaic of photos.
    let mut photo_by_id_depute = HashMap::<String, PhotoDepute>::new();
    let mut rows_filenames: Vec<String> = Vec::new();
    for (row_index, row) in deputes.chunks(25).enumerate() {
        let mut photos_filenames = Vec::<String>::new();
        for (column_index, depute) in row.iter().enumerate() {
            let uid = depute.uid();
            let numero_depute = uid.get(2..).unwrap();
            let photo_filename = format!("{}_150x192.jpg", numero_depute);
            photos_filenames.push(photo_filename.to_string());
            photo_by_id_depute.insert(
                uid.to_string(),
                PhotoDepute {
                    chemin: format!("photos_deputes_{}/{}", legislature, photo_filename),
                    chemin_mosaique: format!("photos_deputes_{}/deputes.jpg", legislature),
                    hauteur: 192,
                    largeur: 150,
                    x_mosaique: column_index as i32 * 150,
                    y_mosaique: row_index as i32 * 192,
                },
            );
        }
        let row_filename = format!("row-{}.jpg", row_index);
        let output = Command::new("gm")
            .current_dir(&photos_dir)
            .arg("convert")
            .args(&photos_filenames)
            .arg("+append")
            .arg(&row_filename)
            .output()
            .expect("Gm row creation failed");
        if !output.status.success() {
            println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
            println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
            panic!("Gm row creation failed");
        }
        rows_filenames.push(row_filename);
    }
    let output = Command::new("gm")
        .current_dir(&photos_dir)
        .arg("convert")
        .args(&rows_filenames)
        .arg("-append")
        .arg("deputes.jpg")
        .output()
        .expect("Gm mosaic creation failed");
    if !output.status.success() {
        println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
        println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
        panic!("Gm mosaic creation failed");
    }
    for row_filename in rows_filenames {
        fs::remove_file(photos_dir.join(&row_filename)).expect("Row image removal failed");
    }

    let json_file_path = photos_dir.join("deputes.json");
    let json_file = File::create(&json_file_path).unwrap();
    serde_json::to_writer_pretty(json_file, &photo_by_id_depute).unwrap();
}
